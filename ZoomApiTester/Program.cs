﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace MyApp // Note: actual namespace depends on the project name.
{
    public class ZoomToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            var clientId = "Imed34cpTzidlPIXwdE74w";
            var clientSecret = "2P9zZajyAjD3vWsJEqIL4jaXQ1PvGuJc";
            var accountId = "p7OLyiKGSNyDmPdq151sGQ";

            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri($"https://zoom.us/oauth/token?grant_type=account_credentials&account_id={accountId}"),
            };

            var byteArray = Encoding.ASCII.GetBytes($"{clientId}:{clientSecret}");
            request.Headers.Host = "zoom.us";
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            var response = client.SendAsync(request).Result;

            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                var zoomToken = JsonConvert.DeserializeObject<ZoomToken>(content);

                HttpClient authedClient = new HttpClient();
                request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri("https://api.zoom.us/v2/users")
                };
                request.Headers.Host = "api.zoom.us";
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", zoomToken.AccessToken);
                // authedClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + zoomToken.AccessToken);

                var res = authedClient.SendAsync(request).Result;
                res.EnsureSuccessStatusCode();

                var responseBody = res.Content.ReadAsStringAsync().Result;
        
                Console.WriteLine(responseBody);
                //Console.WriteLine(content);
            }
            else
            {
                Console.WriteLine($"Error: {response.StatusCode}");
            }
        }
    }
}